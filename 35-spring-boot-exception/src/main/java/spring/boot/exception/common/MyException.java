package spring.boot.exception.common;

public class MyException extends Exception {

    public MyException(String message){
        super(message);
    }

}
