package spring.boot.async.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class HelloService {
    private static final Log logger = LogFactory.getLog(HelloService.class);

    @Async
    public void out(){
        for (int i = 0; i < 10; i++) {
            logger.info("hello service " + i);
        }
    }

}


