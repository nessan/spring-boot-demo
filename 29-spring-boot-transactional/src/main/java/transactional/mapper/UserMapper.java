package transactional.mapper;

import org.apache.ibatis.annotations.Update;
import transactional.entity.User;

public interface UserMapper {

    @Update("update user set name=#{name} where id=#{id}")
    public Integer updateById(User user);
}
