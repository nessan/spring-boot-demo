package spring.rabbitmq.object;


import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ObjectRabbitConfig {

    @Bean
    public Queue Queue() {
        return new Queue("object");
    }


}
