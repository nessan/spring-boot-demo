package spring.rabbitmq.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = TopicRabbitConfig.QUEUE_A)
public class TopicReceiverA {
    @RabbitHandler
    public void process(String message) {
        System.out.println("ReceiverA : " + message);
    }
}
