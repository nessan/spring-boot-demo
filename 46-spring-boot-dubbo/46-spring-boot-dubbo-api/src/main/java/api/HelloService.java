package api;

/**
 * @author blues
 */
public interface HelloService {

    String hello(String name);

}
