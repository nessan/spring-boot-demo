package out.config.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guo
 */
@RestController
public class ConfigController {

    @Value("${name}")
    private String name;

    @GetMapping("hello")
    public String hello(){
        return "hello "+name;
    }

}
