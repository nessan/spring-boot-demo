package mp.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import mp.entity.User;
import mp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/insert/{id}/{name}")
    public Boolean insert(@PathVariable String id, @PathVariable String name){
        User user = User.builder().id(id).name(name).build();
        return userService.insert(user);
    }

    @DeleteMapping("/delete/{id}")
    public Boolean delete(@PathVariable String id){
        return userService.deleteById(id);
    }

    @PutMapping("/update/{id}/{name}")
    public Boolean update(@PathVariable String id, @PathVariable String name){
        User user = new User(id,name);
        return userService.updateById(user);
    }

    @GetMapping("/selectById/{id}")
    public User selectById(@PathVariable String id){
        return userService.selectById(id);
    }

    @GetMapping("/selectPage/{pageNum}/{pageSize}")
    public Page<User> selectPage(@PathVariable Integer pageNum, @PathVariable Integer pageSize){
        Page<User> userPage = new Page<>(pageNum, pageSize);
        Wrapper<User> wrapper = new EntityWrapper<User>().eq("name","blues");
        return userService.selectPage(userPage, wrapper);
    }

    @GetMapping("/selectByPage/{pageNum}/{pageSize}/{name}")
    public Page<User> selectByPage(@PathVariable Integer pageNum, @PathVariable Integer pageSize, @PathVariable String name){
        Page<User> userPage = new Page<>(pageNum, pageSize);
        return userService.selectByPage(userPage, name);
    }
}
