package spring.batch.config;

import org.springframework.batch.item.validator.ValidatingItemProcessor;
import org.springframework.batch.item.validator.ValidationException;

import spring.batch.entity.SysUser;

/**
 * 数据处理类
 *
 * @author blues
 */
public class CsvItemProcessor  extends ValidatingItemProcessor<SysUser>{
	@Override
	public SysUser process(SysUser item) throws ValidationException {
        // 执行super.proces(item) 才会调用自定义校验器
		super.process(item);
		//处理数据 将名字长度和内容乘二
        String name = item.getName();
		item.setName(name+name);
		return item;
	}
}
