package spring.boot.Log;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author guo
 */
@SpringBootApplication
public class SpringBootLogApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootLogApplication.class, args);
	}
}
