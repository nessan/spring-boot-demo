package jms.activemq.service;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

/**
 * 消费者
 * @author guo
 */
@Service
public class MessageConsumerService {

    /**
     * 进行消息接收处理
     */
    @JmsListener(destination="active.msg.queue")
    public void receiveMessage(String text) {
        System.err.println("【*** 接收消息 ***】::: " + text);
    }
}
