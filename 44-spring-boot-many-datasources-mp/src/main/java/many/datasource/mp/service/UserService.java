package many.datasource.mp.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import many.datasource.mp.entity.User;
import many.datasource.mp.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@DS("slave")
@Service
public class UserService {

    @Autowired(required = false)
    private UserMapper userMapper;



    @DS("master") //默认是master可以省略这个注解,如果在类上注解了其他库，则@DS("master")不能省略
    public int insertUser(User user) {
        return userMapper.insertUser(user.getId(), user.getName());
    }

    @DS("slave_1")
    public List<User> selectUser1() {
        return userMapper.selectUserList();
    }

    @DS("slave_2")
    public List<User>  selectUser2() {
        return userMapper.selectUserList();
    }

    //这个slave随机库
    public List<User>  selectUser3() {
        return userMapper.selectUserList();
    }
}
