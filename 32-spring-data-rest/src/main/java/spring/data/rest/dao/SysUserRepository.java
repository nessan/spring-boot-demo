package spring.data.rest.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import spring.data.rest.model.SysUser;

import java.util.List;

@RepositoryRestResource(path = "users")
public interface SysUserRepository extends JpaRepository<SysUser, Long> {

    /**
     * 根据name查询用户
     * @param name
     * @return
     */
    @RestResource(path="name")
    public List<SysUser> findByName(@Param("name") String name);

    /**
     * 查询name开头的
     * @param name
     * @return
     */
    @RestResource(path="nameStartsWith",rel="nameStartsWith")
    public List<SysUser> findByNameStartsWith(@Param("name") String name);

}
