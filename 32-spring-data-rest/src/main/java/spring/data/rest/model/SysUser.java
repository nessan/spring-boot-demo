package spring.data.rest.model;

import javax.persistence.*;

@Entity
@Table(name = "SYS_USER")
public class SysUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    public SysUser(String name) {
        this.name = name;
    }

    public SysUser(){}

    public SysUser(Long id, String name){
        this.id = id;
        this.name = name;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
