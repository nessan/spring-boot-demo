package jdbc.controller;

import jdbc.entity.User;
import jdbc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("user/{id}")
    public User selectById (@PathVariable(value = "id") String id){
        return userService.selectById(id);
    }

    @GetMapping("users")
    public List<User> selectList (){
        return userService.selectList();
    }

    @PostMapping("user")
    public Integer insert (@RequestBody User user){
        return userService.insert(user);
    }

    @DeleteMapping("user/{id}")
    public Integer deleteById (@PathVariable(value = "id") String id){
        return userService.deleteById(id);
    }

    @PutMapping("user")
    public Integer updateById (@RequestBody User user){
        return userService.updateById(user);
    }

}
