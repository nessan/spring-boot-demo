package spring.boot.mybatis;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author guo
 */
@Mapper
@Repository
public interface SysUserDao {

    @Select("SELECT ID AS \"id\",NAME AS \"name\" FROM SYS_USER WHERE NAME=#{name}")
    public List<SysUser> findList(@Param("name") String name);

}
