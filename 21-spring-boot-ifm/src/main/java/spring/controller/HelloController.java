package spring.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author guo
 */
@RestController
public class HelloController {

    @GetMapping("/hello/interceptor")
    public String helloInterceptor(){
        System.out.println("执行接口：hello interceptor 拦截器");
        return "hello";
    }

    @GetMapping("/hello/interceptor/login")
    public String helloInterceptorLogin(){
        return "login";
    }

    @GetMapping("/filter/hello")
    public String filter(){
        return "filter";
    }

    @GetMapping("/test/hello")
    public String test(){
        return "test";
    }
}
