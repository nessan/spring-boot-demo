package spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * @author guo
 */
@SpringBootApplication
@ServletComponentScan
public class SpringIFMApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringIFMApplication.class, args);
	}
}
