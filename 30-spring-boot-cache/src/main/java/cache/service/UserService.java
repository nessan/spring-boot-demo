package cache.service;

import cache.entity.User;
import cache.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@CacheConfig(cacheNames = {"user"})
public class UserService {

    @Autowired(required = false)
    private UserMapper userMapper;


    @Transactional
    @CachePut(key = "#user.id")
    public User insert(User user){
        userMapper.insert(user);
        return user;
    }

    @Transactional
    @CacheEvict(key = "#id")
    public Integer deleteById(String id){
        return userMapper.deleteById(id);
    }


    @Cacheable(key = "#id")
    public User selectById(String id){
        return userMapper.selectById(id);
    }
}

