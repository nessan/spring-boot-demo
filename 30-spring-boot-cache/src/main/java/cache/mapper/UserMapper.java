package cache.mapper;

import cache.entity.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

public interface UserMapper {

    @Insert("insert into user(id,name) values(#{id},#{name})")
    public Integer insert(User user);

    @Delete("delete from user where id=#{id}")
    public Integer deleteById(String id);

    @Select("select id,name from user where id=#{id}")
    public User selectById(String id);
}
