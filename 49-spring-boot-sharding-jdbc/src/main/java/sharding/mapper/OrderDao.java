package sharding.mapper;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sharding.entity.Order;

@Repository
public interface OrderDao extends JpaRepository<Order,Long> {

}
