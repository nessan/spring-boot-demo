package sharding.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sharding.entity.Order;
import sharding.mapper.OrderDao;

@Service
public class OrderService {

    @Autowired
    private OrderDao orderDao;


    public Order insert(Order order){
        return orderDao.save(order);
    }

    public Order selectById(Long id) {
        return orderDao.findOne(id);
    }
}
