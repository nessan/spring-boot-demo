package ehcache.controller;

import ehcache.model.User;
import ehcache.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/insert/{id}/{name}")
    public User insert(@PathVariable String id, @PathVariable String name){
        User user = new User();
        user.setId(id);
        user.setName(name);
        return userService.insert(user);
    }

    @DeleteMapping("/deleteById/{id}")
    public String deleteById(@PathVariable String id){
        userService.deleteById(id);
        return id;
    }

    @PostMapping("/updateById/{id}/{name}")
    public User updateById(@PathVariable String id, @PathVariable String name){
        User user = new User();
        user.setId(id);
        user.setName(name);
        return userService.updateById(user);
    }

    @GetMapping("/selectById/{id}")
    public User selectById(@PathVariable String id){
        return userService.selectById(id);
    }

    @GetMapping("/selectList")
    public List<User> selectList(){
        return userService.selectList();
    }
}
